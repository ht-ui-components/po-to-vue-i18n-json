# po-to-vue-i18n-json

Convert PO GNU Gettext files to Vue i18n compatible JSON files. It has support for plurals and can be imported as a Node module or be executed directly on the command line.

Usage as a Node module
----------------------

~~~~
import { po2Vuei18nJSON, po2Vuei18nJSONFromUri } from 'po-to-vue-i18n-json';

// Get JSON from PO string
const json = po2Vuei18nJSON('\
  msgctxt "context"\n\
  msgid "test"\n\
  msgstr "prueba"\n\
');

// Get JSON from Uri
const json = po2Vuei18nJSONFromUri('https://example.com/my-po-file.po');
~~~~

Executing from the command line
-------------------------------

NOTE: when executing on the command line, only URI paramaters are supported.

~~~~
$> node ./node-modules/po-to-vue-i18n-json https://example.com/my-po-file.po > result.json;
~~~~

JSON result structure
---------------------

The result JSON has the following structure:

~~~
{
  <msgctxt>: {
    <msgid>: <msgstr>,
  },
  ...
}
~~~

This is done so that the context is taken into account. If context is not provided, msgid is used as context.

Example
-------
Original PO content:
~~~
'\
  msgctxt "context"\n\
  msgid "test"\n\
  msgstr "prueba"\n\
'
~~~

Resulting JSON content:
~~~
{
  context: {
    test: 'prueba',
  },
  ...
}
~~~

Accessing translations from Vue component

~~~
const myTranslatedStr = this.$t('context.test');
~~~

Plural support
--------------
When providing plural translations on the original PO file by specifying the fields "msgid_plural" and "msgstr[n]", the results will have the following structure:

~~~
{
  <msgctxt>: {
    <msgid>: <plural_form_0>|<plural_form_1>|...<plural_form_n>,
  },
  ...
}
~~~

The plural forms are obtained from msgstr array specified by the PO content. If the array is incomplete on the PO file (ex: msgstr[0] and msgstr[2] are defined, but msgstr[1] is not), "msgid_plural" will be used as fallback value.

Example
-------
Original PO content:
~~~
'\
  msgctxt "context"\n\
  msgid "orange"\n\
  msgid_plural "oranges"\n\
  msgstr[0] "naranjas"\n\
  msgstr[1] "naranja"\n\
  msgstr[2] "naranjas"\n\
'
~~~

Resulting JSON content:
~~~
{
  context: {
    orange: 'naranjas|naranja|naranjas',
  },
  ...
}
~~~

Accessing plural translations from Vue component

~~~
let myTranslatedStr = this.$tc('context.orange, 0');
// myTranslatedStr => 'oranges'

myTranslatedStr = this.$tc('context.orange, 1');
// myTranslatedStr => 'orange'
~~~
