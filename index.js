const rp = require('request-promise');
const _ = require('lodash');

function getJSONi18nFrom(poData) {
  const result = {};
  const lines = poData.split('\n');
  // Final empty line is needed to make sure that the last translation of the file is taken into account
  lines.push('');
  let currentEntry = {};
  lines.forEach((line) => {
    const newEntry = getCurrentEntry(currentEntry, line);
    // Check if current entry is complete
    if (Object.keys(currentEntry).length >= Object.keys(newEntry).length) {
      result[currentEntry.msgctxt || currentEntry.msgid] = {
        ...result[currentEntry.msgctxt || currentEntry.msgid],
        ...getEntryValue(currentEntry),
      };
    }
    // Update current entry
    currentEntry = newEntry;
  });
  return result;
}

function parseEntryLine(line, key, entry) {
  const contextResult = new RegExp(`${key} "(.+)"`).exec(line);
  if (contextResult === null) {
    return undefined;
  }
  // Entry has already been filled. The newly parsed data corresponds to a new one.
  if (entry[key] !== undefined) {
    // Empty old entry data
    entry = {};
  }
  entry[key] = contextResult[1];
  return entry;
}

function parseEntryPluralLine(line, entry) {
  const contextResult = new RegExp('msgstr\[(.+)\] "(.+)"').exec(line);
  if (contextResult === null) {
    return undefined;
  }
  if (!entry.msgstr) {
    entry.msgstr = {};
  }
  const pluralIndex = contextResult[1];
  const messageContent = contextResult[2];
  entry.msgstr[pluralIndex] = messageContent;
  return entry;
}

function getCurrentEntry(currentEntry, line) {
  return parseEntryLine(line, 'msgctxt', currentEntry) ||
    parseEntryLine(line, 'msgid', currentEntry) ||
    parseEntryLine(line, 'msgid_plural', currentEntry) ||
    parseEntryLine(line, 'msgstr', currentEntry) ||
    parseEntryPluralLine(line, currentEntry) ||
    currentEntry;
}

function getEntryContent(entry) {
  // Return simple message. No plurals defined
  if (!_.isObject(entry.msgstr) && !entry.msgid_plural) {
    return entry.msgstr;
  // Return fallback plural content (Ex: no oranges, 1 orange, 2 oranges, etc)
  } else if (!_.isObject(entry.msgstr)) {
    return `${entry.msgid_plural}|${entry.msgid}|${entry.msgid_plural}`;
  } else {
  // Return parametric plural content
    let result = '';
    const pluralIndexList = Object.keys(entry.msgstr);
    const lastIndex = Math.max(pluralIndexList);
    for (let i = 0; i <= lastIndex; i++) {
      if (i > 0) {
        result.push('|');
      }
      // Use msgid as fallback value
      const fallbackValue = entry.msgid_plural || entry.msgid;
      result.push(entry.msgstr[i] || fallbackValue);
    }
    return result;
  }
}

function getEntryValue(entry) {
  const entryContent = getEntryContent(entry);
  const result = {};
  result[entry.msgid] = entryContent;
  return result;
}

function po2Vuei18nJSON(poData, printResult) {
  return Promise.resolve()
  .then(() => {
    const parsedData = getJSONi18nFrom(poData);
    if (printResult) {
      console.log(parsedData);
    }
    return parsedData;
  });
}

function po2Vuei18nJSONFromUri(uri, printResult) {
  return rp.get(uri)
  .then((poData) => {
    const parsedData = getJSONi18nFrom(poData);
    if (printResult) {
      console.log(parsedData);
    }
    return parsedData;
  });
}

if (process.argv.length > 2) {
  po2Vuei18nJSONFromUri(process.argv[2], process.argv.length < 4);
}

module.exports.po2Vuei18nJSON = po2Vuei18nJSON;
module.exports.po2Vuei18nJSONFromUri = po2Vuei18nJSONFromUri;
